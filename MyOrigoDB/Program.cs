﻿using Orient.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyOrigoDB
{
    class Program
    {
        static void Main(string[] args)
        {
            OServer server = Server.Connect();

            Console.WriteLine("Checking that databases exists...");


            string[] databases = { "Zloszenie", "Rodzaj"};
            foreach (string database in databases)
            {
                bool dbExists = server.DatabaseExist(database);

                if (dbExists == false)
                {
                    Console.WriteLine("Database {0} doesn't exist, creating...",
                       database);
                    server.CreateDatabase(database,
                       ODatabaseType.Graph,
                       OStorageType.Memory);
                }
                else
                {
                    Console.WriteLine("Database {0} exists already",
                       database);
                }
            }

            Console.ReadKey();
        }
    }
    public static class Server
    {
        private static string hostname = "localhost";
        private static int port = 2424;
        private static string user = "root";
        private static string passwd = "root_passwd";

        public static OServer Connect()
        {
            OServer server = new OServer(hostname, port, user, passwd);
            return server;
        }
    }
}

