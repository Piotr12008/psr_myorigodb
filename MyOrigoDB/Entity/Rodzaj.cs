﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeo4j.Entity
{
    public class Rodzaj
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
    }
}
