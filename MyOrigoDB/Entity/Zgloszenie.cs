﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeo4j.Entity
{
    public class Zgloszenie
    {
        public int Id { get; set; }
        public Rodzaj Rodzaj { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public DateTime Data { get; set; }
        public string Opis { get; set; }
        public bool Zamknieta { get; set; }
    }
}
